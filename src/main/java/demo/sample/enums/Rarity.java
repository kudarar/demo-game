package demo.sample.enums;

public enum Rarity {

	RARE("レア"), //
	SS_RARE("SSレア");

	Rarity(String rarity) {
		this.rarity = rarity;
	}

	private String rarity;

	public String getRarity() {
		return rarity;
	}
}
