package demo.sample.enums;

public enum Menu {

	GACHA(1, "１．ガチャを引く"), //
	SHOW_BOX(2, "２．BOXを見る"), //
	END(9, "９．ゲームを終了する"),//
	;

	private Menu(int menuNo, String title) {
		this.menuNo = menuNo;
		this.title = title;
	}

	private int menuNo;
	private String title;

	public static Menu getMenu(int menuNo) {
		for (Menu menu : Menu.values()) {
			if (menu.menuNo == menuNo) {
				return menu;
			}
		}
		throw new IllegalArgumentException("メニューにない番号が指定されました。");
	}

	public int getMenuNo() {
		return menuNo;
	}

	public String getTitle() {
		return title;
	}
}
