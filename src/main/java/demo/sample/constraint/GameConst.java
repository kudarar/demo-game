package demo.sample.constraint;

public class GameConst {

	public static final int FIRST_TIME_ORB = 300;

	public static final int GACHA_ORB_COST_SINGLE = 5;
	public static final int GACHA_ORB_COST_10REN = GACHA_ORB_COST_SINGLE * 10;

	public static final int GACHA_SINGLE = 1;
	public static final int GACHA_10REN_COUNT = 10;

	public static final float SS_RARE_PROB = 0.10f;

}
