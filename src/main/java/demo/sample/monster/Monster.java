package demo.sample.monster;

import demo.sample.enums.Rarity;

public interface Monster {
	public String showAllStatus();

	public String showStatusForGacha();

	public int getMonsterID();

	public Rarity getRarity();
}
