package demo.sample.monster;

import demo.sample.enums.Rarity;

public class CollaborationMonster extends MonsterBase {

	private String collaborationCompanyName;
	private String adUrl;

	public CollaborationMonster(int id, String name, int attack, Rarity rarity, String collaborationCompanyName,
			String adUrl) {
		super(id, name, attack, rarity);
		this.collaborationCompanyName = collaborationCompanyName;
		this.adUrl = adUrl;
	}

	public String getCollaborationCompanyName() {
		return collaborationCompanyName;
	}

	public String getAdUrl() {
		return adUrl;
	}

	@Override
	public String showStatusForGacha() {
		return String.format("名前：%s, レアリティ：%s , コラボ会社名：%s, キャンペーンリンク：%s",
				this.name, //
				this.rarity.getRarity(), //
				this.collaborationCompanyName, //
				this.adUrl //
		);
	}

}
