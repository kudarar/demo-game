package demo.sample.monster.monsters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import demo.sample.enums.Rarity;
import demo.sample.monster.BasicMonster;
import demo.sample.monster.CollaborationMonster;
import demo.sample.monster.Monster;

public enum Monsters {

	MON_A(new BasicMonster(1, "モンスターA", 100, Rarity.RARE)), //
	MON_B(new BasicMonster(2, "モンスターB", 102, Rarity.SS_RARE)), //
	MON_C_A(new CollaborationMonster(10, "キャンペーンモンスターA", 200, Rarity.SS_RARE, "コラボ", "http://hoge.com")), //
	MON_C_B(new CollaborationMonster(11, "キャンペーンモンスターB", 250, Rarity.SS_RARE, "コラボ", "http://hoge.com")), //
	;

	private Monster monster;

	Monsters(Monster monster) {
		this.monster = monster;
	}

	public static Monster getMonster(int id) {
		for (Monsters m : Arrays.asList(Monsters.values())) {
			if (m.monster.getMonsterID() == id) {
				return m.monster;
			}
		}
		throw new IllegalArgumentException(String.format("存在しないモンスターID: %s", id));
	}

	public static List<Monster> getMonster(Rarity rarity) {
		List<Monster> list = new ArrayList<>();

		for (Monsters m : Arrays.asList(Monsters.values())) {
			if (m.monster.getRarity() == rarity) {
				list.add(m.monster);
			}
		}
		return list;
	}

}
