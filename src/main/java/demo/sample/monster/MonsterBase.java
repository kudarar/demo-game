package demo.sample.monster;

import demo.sample.enums.Rarity;

public abstract class MonsterBase implements Monster {

	protected int id;
	protected String name;
	protected int attack;
	protected Rarity rarity;

	public MonsterBase(int id, String name, int attack, Rarity rarity) {
		this.id = id;
		this.name = name;
		this.attack = attack;
		this.rarity = rarity;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getAttack() {
		return attack;
	}

	public Rarity getRarity() {
		return rarity;
	}

	@Override
	public String showAllStatus() {
		return String.format("ID：%s, 名前：%s, 攻撃力：%s, レア度：%s",
				this.id,
				this.name, //
				this.attack, //
				this.rarity.getRarity() //
		);
	}

	@Override
	public String showStatusForGacha() {
		return String.format("名前：%s, レアリティ：%s",
				this.name, //
				this.rarity.getRarity() //
		);
	}

	@Override
	public int getMonsterID() {
		return this.id;
	}

}
