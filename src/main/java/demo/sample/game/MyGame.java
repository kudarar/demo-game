package demo.sample.game;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import demo.sample.enums.Menu;
import demo.sample.exception.ShortOrbException;
import demo.sample.monster.Monster;
import demo.sample.user.User;

public class MyGame {

	private User user;

	MyGame(User user) {
		this.user = user;
	}

	public static void main(String[] args) {
		MyGame game = new MyGame(new User());
		game.start();
	}

	void start() {
		while (true) {
			int menuNo = this.selectMenu();
			if (playMenu(menuNo) == false) {
				break;
			}
			System.out.println("\r\n");
		}
	}

	int selectMenu() {

		// トップナビゲーション
		String navi = "メニューを番号選んで下さい（所有オーブ数：%s個）";
		System.out.println(String.format(navi, user.getOrb()));

		// メニューリスト
		Arrays.asList(Menu.values()).forEach(m -> System.out.println(m.getTitle()));

		// 入力受付
		System.out.print("メニュー番号 : ");
		Scanner scan = new Scanner(System.in);
		int menuNo = scan.nextInt();

		return menuNo;
	}

	boolean playMenu(int menuNo) {

		Menu menu = Menu.getMenu(menuNo);

		switch (menu) {
		case GACHA:
			play10RenGacha();
			return true;

		case SHOW_BOX:
			showBox();
			return true;

		case END:
			System.out.println("★Thank You For Playing!★");
			return false;

		default:
			return true;
		}

	}

	void play10RenGacha() {
		try {
			// ユーザはガチャを引いて
			List<Monster> monsters = user.gacha10Ren();

			// ガチャの結果表示
			System.out.println("１０連ガチャ！");
			int cnt = 1;
			for (Monster m : monsters) {
				System.out.println(String.format("%d回目 %s", cnt, m.showStatusForGacha()));
				cnt++;
			}

			// ガチャのモンスターをユーザのボックスに格納する
			user.addMonster(monsters);

		} catch (ShortOrbException e) {
			System.out.println("オーブが足りないよ！課金してｗ");
		}
	}

	void showBox() {

		List<Monster> monsters = user.hasBoxMonsters();
		if (monsters.isEmpty()) {
			System.out.println("BOXが空です。");
		} else {
			user.hasBoxMonsters().forEach(monster -> System.out.println(monster.showAllStatus()));
		}

	}

}
