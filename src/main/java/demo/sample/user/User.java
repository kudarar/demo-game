package demo.sample.user;

import java.util.List;

import demo.sample.box.MonsterBox;
import demo.sample.constraint.GameConst;
import demo.sample.exception.ShortOrbException;
import demo.sample.gacha.Gacha;
import demo.sample.monster.Monster;

public class User {

	private int orb = GameConst.FIRST_TIME_ORB;
	private final MonsterBox monsterBox = MonsterBox.createMonsterBox();

	public int getOrb() {
		return orb;
	}

	public Monster gachaSingle() throws ShortOrbException {
		if (this.orb >= GameConst.GACHA_ORB_COST_SINGLE) {
			this.orb -= GameConst.GACHA_ORB_COST_SINGLE;
			return new Gacha().gachaSingle();
		}
		throw new ShortOrbException();
	}

	public List<Monster> gacha10Ren() throws ShortOrbException {
		if (this.orb >= GameConst.GACHA_ORB_COST_10REN) {
			this.orb -= GameConst.GACHA_ORB_COST_10REN;
			return new Gacha().gacha10Ren();
		}
		throw new ShortOrbException();
	}

	public void addMonster(List<Monster> monsters) {
		this.monsterBox.add(monsters);
	}

	public List<Monster> hasBoxMonsters() {

		return monsterBox.getMonsterList();
	}
}
