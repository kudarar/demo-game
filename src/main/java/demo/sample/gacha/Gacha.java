package demo.sample.gacha;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import demo.sample.constraint.GameConst;
import demo.sample.enums.Rarity;
import demo.sample.monster.Monster;
import demo.sample.monster.monsters.Monsters;

public class Gacha {

	public Monster gachaSingle() {

		List<Monster> list = Monsters.getMonster(getRarity());
		int randomIndex = new Random().nextInt(list.size());
		return list.get(randomIndex);
	}

	public List<Monster> gacha10Ren() {

		List<Monster> list = new ArrayList<>();
		for (int i = 0; i < GameConst.GACHA_10REN_COUNT; i++) {
			list.add(gachaSingle());
		}
		return list;
	}

	private Rarity getRarity() {
		return new Random().nextFloat() <= GameConst.SS_RARE_PROB ? Rarity.SS_RARE : Rarity.RARE;
	}
}
